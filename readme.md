# Muskland

This is a tribute for Elon Musk for his passion, commitment and contribution towards humanity, technology, and society.

You can view the live website [here](https://optimistic-almeida-72f9e2.netlify.com/).

## Getting Started

If you wish to run this in your local system, please clone the repo and follow developement instructions.

### Prerequisites

To run this in your local system, your will require

- NodeJS
- Yarn or NPM

### Installing

- Clone the Repo
- cd into the repo and run 'yarn install or npm install' to install all the neccessary dependencies'
- Then run 'yarn run dev' for development and 'yarn run build' for production.

## Deployment

Please contact or raise an issue if you find any bug in this site.

## Built With

- [HTML](https://www.w3.org/html/) - The web framework used
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html) - Dependency Management
- [SCSS](https://sass-lang.com/) - Used to generate RSS Feeds
- [PARCELJS](https://parceljs.org/) - Used to generate RSS Feeds

## Acknowledgments

- Elon Musk
- Wikipedia
- Images credits belong to creator/owner.
